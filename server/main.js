import {Meteor} from 'meteor/meteor';

import Products from '../imports/api/products';
import Users from '../imports/api/users';
import Orders from '../imports/api/orders';

import '../imports/api/grapher/links';

if (Meteor.isServer) {
  Meteor.publish ('users', function () {
    return Users.find ();
  });
  Meteor.publish ('orders', function () {
    return Orders.find ({
      userId: this.userId,
    });
  });
  Meteor.publish ('products', function () {
    return Products.find ();
  });
}

Meteor.startup (() => {
  // code to run on server at startup

  Meteor.methods ({
    upsertDocument: function (collectionName, id, doc) {
      try {
        if (collectionName === 'users') Users.upsert (id, doc);
        if (collectionName === 'products') Products.upsert (id, doc);
        if (collectionName === 'orders') Orders.upsert (id, doc);
      } catch (err) {
        throw new Meteor.Error ('Could not save', err);
      }
      return 'success';
    },

    findOrders: function (query) {
      const createdQuery = Orders.createQuery ({
        $filters: {...query, userId: this.userId},
        user: {
          firstName: 1,
          lastName: 1,
        },
        products: {
          name: 1,
          price: 1,
          quantity: 1,
        },
      });
      return createdQuery.fetch ();
    },
  });
});
