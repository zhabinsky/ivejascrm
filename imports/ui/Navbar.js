import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import {withTracker} from 'meteor/react-meteor-data';

const styles = {
  link: {
    margin: 5,
    paddingTop: 10,
    paddingBottom: 10,
  },
  li: {
    marginTop: 0,
    paddingTop: 0,
    paddingLeft: 8,
  },
};

class Navbar extends Component {
  render () {
    const pathname = window.location.pathname;

    const renderLoginData = () => {
      const user = this.props.user;
      //   console.log (user);
      return (
        <div
          className="round"
          style={{
            backgroundColor: user ? '#81c784' : '#e57373',
            padding: 20,
            color: 'white',
            marginBottom: 20,
          }}
        >
          {!user
            ? 'Not logged in'
            : <React.Fragment>
                ID: {user._id}
                {' '}
                {(user.emails || []).map (email => email.address)}
              </React.Fragment>}
        </div>
      );
    };

    return (
      <React.Fragment>
        {/* <div>{JSON.stringify ()}</div> */}
        {renderLoginData ()}
        <ul>
          {[
            //   {text: 'Dashboard', url: '/dashboard', icon: 'signal'},
            {text: 'Customers', url: '/customers', icon: 'user'},
            {text: 'Products', url: '/products', icon: 'box'},
            {text: 'Orders', url: '/orders', icon: 'credit-card'},
            {text: 'About', url: '/about', icon: 'address-book'},
          ].map ((menuItem, index) => {
            const isCurrentRoute = pathname === menuItem.url;
            // console.log (
            //   'is current route ',
            //   isCurrentRoute,
            //   ':  ',
            //   pathname,
            //   menuItem.url
            // );
            return (
              <React.Fragment key={index}>

                <li
                  className="hoverbg round"
                  style={{
                    ...styles.li,
                    backgroundColor: isCurrentRoute ? '#ffecb3' : undefined,
                  }}
                >
                  <Link
                    className="flexrowstart"
                    to={menuItem.url}
                    style={{
                      ...styles.link,
                    }}
                    onClick={() => {
                      this.setState ({
                        a: Math.random (),
                      });
                      // navbar needs to re-render on route changes
                    }}
                  >
                    <div style={{width: '50px'}} className="">
                      <i className={'fas fa-' + menuItem.icon} />
                    </div>
                    <span>{menuItem.text}</span>
                  </Link>
                </li>
              </React.Fragment>
            );
          })}
        </ul>
      </React.Fragment>
    );
  }
}

export default withTracker (() => {
  return {
    user: Meteor.user (),
  };
}) (Navbar);
