import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';

import Navbar from './Navbar.js';
import AccountsUI from './AccountsUI';

import TableViewer from './TableViewer';

import Users from '../api/users';
import Orders from '../api/orders';
import Products from '../api/products';

import {withTracker} from 'meteor/react-meteor-data';

class App extends Component {
  constructor () {
    super ();
  }

  render = () => {
    const renderArrayOfStrings = arr => {
      return (
        <div>
          {(arr || []).map ((entry, index) => {
            return (
              <div key={index}>
                {entry}
              </div>
            );
          })}
        </div>
      );
    };

    return (
      <Router>
        <React.Fragment>

          <div style={{marginBottom: 20}}>

            <AccountsUI />
          </div>

          <div
            className="container"
            style={{
              display: 'grid',
              gridTemplateColumns: '1fr 3fr',
            }}
          >
            <div><Navbar user={this.props.user} /></div>
            <div
              className="round"
              style={{
                marginTop: 5,
                marginLeft: 5,
                padding: 10,
                //   backgroundColor: '#ffccbc',
              }}
            >
              <Switch>
                <Route
                  exact
                  path="/products"
                  component={() => {
                    return (
                      <TableViewer
                        title={'Products'}
                        collection={Products}
                        fields={[
                          {accessor: 'name', lookForSuggestions: true},
                          {accessor: 'price'},
                          {accessor: 'quantity'},
                        ]}
                      />
                    );
                  }}
                />
                <Route
                  exact
                  path="/orders"
                  component={() => {
                    return (
                      <TableViewer
                        customQuery={'findOrders'}
                        title={'Orders that belong to me'}
                        collection={Orders}
                        fields={[
                          {
                            accessor: 'user',
                            render: (user = {}) => {
                              return (
                                <div>
                                  <div>FirstName: {user.firstName}</div>
                                  <div>LastName: {user.lastName}</div>
                                </div>
                              );
                            },
                          },
                          {
                            accessor: 'products',
                            render: (products = []) => {
                              return (
                                <div>
                                  {products.map ((product, index) => {
                                    return (
                                      <div
                                        key={index}
                                        style={{border: '1px solid gray'}}
                                      >
                                        <div>ProductName: {product.name}</div>
                                        <div>Quantity: {product.quantity}</div>
                                        <div>Price: {product.price}</div>
                                      </div>
                                    );
                                  })}
                                </div>
                              );
                            },
                          },
                        ]}
                      />
                    );
                  }}
                />
                <Route
                  exact
                  path="/customers"
                  component={() => {
                    return (
                      <TableViewer
                        title={'Customers'}
                        collection={Users}
                        fields={[
                          {accessor: 'firstName', lookForSuggestions: true},
                          {accessor: 'lastName', lookForSuggestions: true},
                          {
                            accessor: 'emails',
                            render: value => {
                              return (
                                <React.Fragment>
                                  {(value || []).map (email => {
                                    return (
                                      <div key={email.address}>
                                        {email.address}
                                      </div>
                                    );
                                  })}
                                </React.Fragment>
                              );
                            },
                          },
                          {accessor: 'age'},
                          {
                            accessor: 'isBlocked',
                            render: value => (
                              <i
                                className={
                                  value ? 'fas fa-check-circle' : 'fas fa-times'
                                }
                              />
                            ),
                          },
                        ]}
                      />
                    );
                  }}
                />
                <Route
                  exact
                  path="/about"
                  component={() => (
                    <div>
                      <table>
                        <tbody>

                          {[
                            [
                              'Blaze templates with reactivity',
                              'Used React instead',
                            ],
                            ['• AutoForm', 'Done'],
                            ['• FlowRouter', 'Used React router v4 instead'],
                            ['• Pagination, or infinite list', 'Pagination'],
                            [
                              '• User data validation with SimpleSchema and validated methods',
                              'autoform does the validation',
                            ],
                            [
                              '• Exception handing at the client side (alert system, e.g. Bert)',
                              'Done, used Bert',
                            ],
                            [
                              '• Joins via Grapher',
                              'used createQuery() to fetch orders',
                            ],
                            [
                              '• Meteor Accounts and Allow/Deny rules',
                              'Acounts - Done, rules = ___TODO',
                            ],
                            ['• Bootstrap 4', 'didnt use'],
                            [
                              '• Reactive fuzzy search of customers/order attributes (based on Levenshtein distance)',
                              <div>
                                {'Done, used: '}
                                {' '}
                                <a
                                  href="https://github.com/perak/fuzzy-search"
                                  style={{
                                    textDecoration: 'underline',
                                    color: 'blue',
                                  }}
                                >
                                  fuzzy
                                </a>
                              </div>,
                            ],
                          ].map (e => {
                            return (
                              <tr key={e[0]}>
                                <th>{e[0]}</th>
                                <th>{e[1]}</th>
                              </tr>
                            );
                          })}

                        </tbody>
                      </table>
                    </div>
                  )}
                />
              </Switch>
            </div>
          </div>

        </React.Fragment>
      </Router>
    );
  };
}
export default withTracker (() => {
  Meteor.subscribe ('users');
  Meteor.subscribe ('orders');
  Meteor.subscribe ('products');

  return {};
}) (App);
