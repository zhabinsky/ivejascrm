import React, {Component} from 'react';

// Task component - represents a single todo item
export default class Task extends Component {
  render () {
    return (
      <li>
        <div>{this.props.task.text}</div>
        {' '}
        <div>
          {this.props.task.createdAt && this.props.task.createdAt.toString ()}
        </div>
      </li>
    );
  }
}
