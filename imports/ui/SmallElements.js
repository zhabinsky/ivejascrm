import React, {Component} from 'react';
import Radium from 'radium';

const colors = [
  '#ffcdd2',
  '#bbdefb',
  '#dcedc8',
  '#ffd6c9',
  '#b2ebf2',
  '#ffecb3',
  '#b2dfdb',
];

const dimensions = {
  buttonWidth: '43px',
};

const getColor = colorCode => {
  return colors[colorCode % colors.length];
};

const RoundButton = Radium (
  ({text = '', colorCode = 0, icon = '', onClick = () => {}}) => {
    const backgroundColor = getColor (colorCode);
    return (
      <div
        onClick={onClick}
        style={{
          marginLeft: '5px',
          backgroundColor,
          width: dimensions.buttonWidth,
          height: dimensions.buttonWidth,
          borderRadius: '20%',
          transition: 'all 0.2s ease-out',
          ':hover': {
            boxShadow: '0 3px 6px rgba(0, 0, 0, 0.16), 0 3px 6px rgba(0, 0, 0, 0.23)',
          },
        }}
        className="flexrowcenter"
      >
        <i
          className={'fas fa-' + icon}
          style={{
            //   color: 'white'
          }}
        />
      </div>
    );
  }
);

export {RoundButton, colors, getColor};
