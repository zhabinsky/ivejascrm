import {Mongo} from 'meteor/mongo';
import SimpleSchema from 'simpl-schema';

const Products = new Mongo.Collection ('products');
Products.schema = new SimpleSchema ({
  name: {type: String},
  price: {type: Number},
  quantity: {type: Number},
});

export default Products;
