import {Mongo} from 'meteor/mongo';
import SimpleSchema from 'simpl-schema';
import Products from './products';

const Orders = new Mongo.Collection ('orders');
const Users = Meteor.users;

SimpleSchema.extendOptions (['uniforms']);
Orders.schema = new SimpleSchema ({
  userId: {
    type: String,
    uniforms: {
      options: function () {
        return Users.find ({}).map (function (c) {
          return {
            label: c.firstName +
              ' ' +
              c.lastName +
              ' ' +
              JSON.stringify (c.emails),
            value: c._id,
          };
        });
      },
    },
  },
  productIds: {
    type: Array,
    uniforms: {
      options: function () {
        return Products.find ({}).map (function (c) {
          return {
            label: c.name,
            value: c._id,
          };
        });
      },
    },
  },
  'productIds.$': {
    type: String,
  },
});

export default Orders;
