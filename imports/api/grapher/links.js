import Users from '../users';
import Orders from '../orders';
import Products from '../products';

// Users.addLinks ({
//   orders,
// });

Orders.addLinks ({
  user: {
    collection: Users,
    field: 'userId',
    type: 'one',
  },
  products: {
    collection: Products,
    field: 'productIds',
    type: 'many',
  },
});
