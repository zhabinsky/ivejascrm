import SimpleSchema from 'simpl-schema';

const Users = Meteor.users;

Users.schema = new SimpleSchema (
  {
    firstName: {type: String},
    lastName: {type: String},
    emails: {
      type: Array,
    },

    'emails.$': {
      type: Object,
      optional: false,
    },
    'emails.$.address': {
      type: String,
      optional: false,
    },
    age: {type: Number},
    isBlocked: {type: Boolean, optional: true},
  },
  {
    clean: {
      trimStrings: false,
    },
  }
);

SimpleSchema.extendOptions (['autoform']);

export default Users;
